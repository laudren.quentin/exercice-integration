
/* Menu Burger */

let btnBurger = document.getElementById('btnMenuBurger');

btnBurger.addEventListener('click', () => {
    let body = document.body,
        nav = document.getElementById('nav'),
        wrpBody = document.getElementById('wrpBody');

    if (nav.classList.contains('openNav')) {
        nav.classList.remove('openNav');
        body.classList.remove('openBody');
        btnBurger.classList.remove('oppen');
        wrpBody.classList.remove('wrapOpenMenuBodyOn');


    } else {
        nav.classList.add('openNav');
        body.classList.add('openBody');
        btnBurger.classList.add('oppen');
        wrpBody.classList.add('wrapOpenMenuBodyOn');
    }
});


/* Vérif Form */
let inputName = document.getElementById('nameInput'),
    errorName = document.getElementById('errorNameInput'),
    inputMail = document.getElementById('emailInput'),
    errorMail = document.getElementById('errorMailInput'),
    inputTextArea = document.getElementById('contentInput'),
    errorTxtArea = document.getElementById('errorTextarea'),
    btnSubmitForm = document.getElementById('btnSubmitForm'),

    regName = new RegExp(/^([a-zA-Z_-])+$/),
    regMail = new RegExp(/^(.+@.+\..+)$/);

let ErrorInputEraseText = function (evt) {
    console.log(this.parentNode.childNodes)
    let caca = this.parentNode.childNodes[3];
    caca.classList.remove("error");
    caca.textContent = '';
};
inputName.addEventListener('focus', ErrorInputEraseText);
inputTextArea.addEventListener('focus', ErrorInputEraseText);
inputMail.addEventListener('focus', ErrorInputEraseText);

let controleForm = function (evt) {
    evt.preventDefault();
    let hasError = false;
    console.log('prout');
    errorName.textContent = '';
    errorMail.textContent = '';
    errorTxtArea.textContent = '';
    /* Input du nom */
    if (regName.test(inputName.value) !== true) {
        errorName.textContent = 'les lettres anonyme c\'est mal';
        errorName.classList.add('error');
        inputName.value = '';
        hasError = true;
    }
    /* Input du mail */
    if (regMail.test(inputMail.value) !== true) {
        errorMail.textContent = 'et comment on fait pour vous spammer ?';
        errorMail.classList.add('error');
        inputMail.value = '';
        hasError = true;
    }
    /* Text Area */
    if (inputTextArea.value == 0) {
        errorTxtArea.textContent = "Oh Fada t'y a craqué ton slip ou quoi ?";
        errorTxtArea.classList.add('error');
        errorTxtArea.value = '';
        hasError = true;

    }

    /* si il y a une érreur on n'envoie rien */
    if (hasError) {
        return;
    }
    if (hasError === false) {

        /* Si tout est bon on envoit la modale */
        let divModale = document.getElementById('modaleContainer');
        let modaleHtml = '';
        modaleHtml += '<div class="flexModale">';
        modaleHtml += '<div class="modale">';
        modaleHtml += '<p class="modale-text">On vous avait prévenu qu\'il allait faire tout noir...<br> Votre message en a profité pour s\'enfuit jusqu\'a chez nous !</p>';
        modaleHtml += '<div class="container-btn">';
        modaleHtml += '<button type="button" class="btnModel" id="modale-btn">';
        modaleHtml += '<i class="fas fa-check"></i>J\'ai peur du noir :\'(';
        modaleHtml += '</button>';
        modaleHtml += '</div>';
        modaleHtml += '</div>';
        modaleHtml += '</div>';

        divModale.style.display = 'block'
        divModale.innerHTML = modaleHtml;
        document.body.append(divModale);

        const btnModal = document.getElementById('modale-btn');

        btnModal.addEventListener('click', () => {
            if (divModale.style.display = 'block') {
                divModale.style.display = 'none';
            }
        });
    }


}

btnSubmitForm.addEventListener('click', controleForm);
